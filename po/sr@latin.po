# Serbian translation of network-manager-fortisslvpn.
# Courtesy of Prevod.org team (http://prevod.org/) -- 2012—2017.
# This file is distributed under the same license as the PACKAGE package.
# Miroslav Nikolić <miroslavnikolic@rocketmail.com>, 2012—2017.
msgid ""
msgstr ""
"Project-Id-Version: NetworkManager-fortisslvpn\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/NetworkManager-"
"fortisslvpn/\n"
"POT-Creation-Date: 2022-03-11 17:27+0100\n"
"PO-Revision-Date: 2017-08-13 10:26+0200\n"
"Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>\n"
"Language-Team: srpski <gnome-sr@googlegroups.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Project-Style: gnome\n"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:9
msgid "Fortinet SSLVPN client"
msgstr "Fortinet SSLVPN klijent"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:10
msgid "Client for Fortinet SSLVPN virtual private networks"
msgstr "Klijent za Fortinet SSLVPN virtuelne privatne mreže"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:24
msgid ""
"Support for configuring Fortinet SSLVPN virtual private network connections."
msgstr "Podrška za podešavanje veza Fortinet SSLVPN virtuelne privatne mreže."

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:32
#, fuzzy
msgid "The advanced options dialog"
msgstr "Napredne opcije za SSLVPN"

#: appdata/network-manager-fortisslvpn.metainfo.xml.in:41
msgid "The NetworkManager Developers"
msgstr "Programeri Upravnika mreže"

#. Otherwise, we have no saved password, or the password flags indicated
#. * that the password should never be saved.
#.
#: auth-dialog/main.c:165
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr ""
"Morate da potvrdite identitet da biste mogli da pristupite Virtuelnoj "
"privatnoj mreži „%s“."

#: auth-dialog/main.c:174 auth-dialog/main.c:198
msgid "Authenticate VPN"
msgstr "Potvrdi VPN"

#: auth-dialog/main.c:177
#, fuzzy
msgid "Password"
msgstr "Lozinka"

#: auth-dialog/main.c:179
#, fuzzy
msgid "Token"
msgstr "Prsten"

#: auth-dialog/main.c:202
#, fuzzy
msgid "_Token:"
msgstr "_Prsten"

#: properties/nm-fortisslvpn-editor-plugin.c:38
msgid "Fortinet SSLVPN"
msgstr "Fortinet SSLVPN"

#: properties/nm-fortisslvpn-editor-plugin.c:39
msgid "Compatible with Fortinet SSLVPN servers."
msgstr "Saglasno sa Fortinet SSLVPN serverima."

#: shared/nm-fortissl-properties.c:125
#, c-format
msgid "invalid gateway “%s”"
msgstr "neispravan mrežni prolaz „%s“"

#: shared/nm-fortissl-properties.c:133
#, c-format
msgid "invalid certificate authority “%s”"
msgstr "neispravan izdavač uverenja „%s“"

#: shared/nm-fortissl-properties.c:147
#, c-format
msgid "invalid integer property “%s”"
msgstr "neispravno svojstvo celog broja „%s“"

#: shared/nm-fortissl-properties.c:157
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr "neispravno logičko svojstvo „%s“ (nije „da“ ili „ne“)"

#: shared/nm-fortissl-properties.c:164
#, c-format
msgid "unhandled property “%s” type %s"
msgstr "nerukovano svojstvo „%s“ vrste %s"

#: shared/nm-fortissl-properties.c:175
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "svojstvo „%s“ je neispravno ili nije podržano"

#: shared/nm-fortissl-properties.c:192
msgid "No VPN configuration options."
msgstr "Nema opcija za VPN podešavanje."

#: shared/nm-fortissl-properties.c:212
#, c-format
msgid "Missing required option “%s”."
msgstr "Nedostaje zatražena opcija „%s“."

#: shared/nm-fortissl-properties.c:236
msgid "No VPN secrets!"
msgstr "Nema VPN tajni!"

#: shared/nm-utils/nm-shared-utils.c:264
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "klasa objekta „%s“ nema svojstvo sa nazivom „%s“"

#: shared/nm-utils/nm-shared-utils.c:271
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "svojstvo „%s“ klase objekta „%s“ nije upisivo"

#: shared/nm-utils/nm-shared-utils.c:278
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr "svojstvo tvorbe „%s“ za objekat „%s“ se ne može postaviti nakon tvorbe"

#: shared/nm-utils/nm-shared-utils.c:286
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr "„%s::%s“ nije ispravan naziv svojstva; „%s“ nije podvrsta Gobjekta"

#: shared/nm-utils/nm-shared-utils.c:295
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr "ne mogu da postavim svojstvo „%s“ vrste „%s“ iz vrednosti vrste „%s“"

#: shared/nm-utils/nm-shared-utils.c:306
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr ""
"vrednost „%s“ vrste „%s“ nije ispravna ili je van opsega za svojstvo „%s“ "
"vrste „%s“"

#: shared/nm-utils/nm-vpn-plugin-utils.c:69
#, fuzzy, c-format
msgid "unable to get editor plugin name: %s"
msgstr "ne mogu da učitam priključak uređivača: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:103
#, c-format
msgid "missing plugin file \"%s\""
msgstr "nedostaje datoteka priključka „%s“"

#: shared/nm-utils/nm-vpn-plugin-utils.c:109
#, c-format
msgid "cannot load editor plugin: %s"
msgstr "ne mogu da učitam priključak uređivača: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:118
#, c-format
msgid "cannot load factory %s from plugin: %s"
msgstr "ne mogu da učitam fabriku „%s“ iz priključka: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:144
msgid "unknown error creating editor instance"
msgstr "nepoznata greška stvaranja primerka uređivača"

#: src/nm-fortisslvpn-service.c:218
msgid "Could not find the openfortivpn binary."
msgstr "Ne mogu da nađem izvršnu „openfortivpn“-a."

#: src/nm-fortisslvpn-service.c:402
msgid "Missing VPN username."
msgstr "Nedostaje VPN korisničko ime."

#: src/nm-fortisslvpn-service.c:411
msgid "Missing or invalid VPN password."
msgstr "Nedostaje ili je neispravna VPN lozinka."

#: src/nm-fortisslvpn-service.c:536
msgid "Got new secrets, but nobody asked for them."
msgstr ""

#: src/nm-fortisslvpn-service.c:720
msgid "Don’t quit when VPN connection terminates"
msgstr "Ne prekida kada se završava VPN veza"

#: src/nm-fortisslvpn-service.c:721
msgid "Enable verbose debug logging (may expose passwords)"
msgstr ""
"Uključuje opširno zapisivanje zarad ispravljanja grešaka (može da izloži "
"lozinke)"

#: src/nm-fortisslvpn-service.c:722
msgid "D-Bus name to use for this instance"
msgstr "Naziv D-sabirnice koji će se koristiti za ovaj primerak"

#: src/nm-fortisslvpn-service.c:743
msgid ""
"nm-fortisslvpn-service provides integrated SSLVPN capability (compatible "
"with Fortinet) to NetworkManager."
msgstr ""
"Usluga um-fortisslvpn obezbeđuje objedinjenu SSLVPN mogućnost (saglasno sa "
"Fortinet-om) Upravniku mreže."

#: properties/nm-fortisslvpn-dialog.ui:7
msgid "SSLVPN Advanced Options"
msgstr "Napredne opcije za SSLVPN"

#: properties/nm-fortisslvpn-dialog.ui:50
#: properties/nm-fortisslvpn-dialog.ui:351
msgid "Authentication"
msgstr "Prijava"

#: properties/nm-fortisslvpn-dialog.ui:67
msgid "_Realm"
msgstr ""

#: properties/nm-fortisslvpn-dialog.ui:81
#: properties/nm-fortisslvpn-dialog.ui:337
msgid ""
"SSLVPN server IP or name.\n"
"config: the first parameter of fortisslvpn"
msgstr ""
"IP ili naziv SSLVPN servera.\n"
"podešavanje: prvi parametar fortisslvpn-a"

#: properties/nm-fortisslvpn-dialog.ui:95
msgid "Security"
msgstr "Bezbednost"

#: properties/nm-fortisslvpn-dialog.ui:113
#, fuzzy
msgid "Trusted _certificate"
msgstr "Poverljivo uverenje"

#: properties/nm-fortisslvpn-dialog.ui:127
msgid ""
"A SHA256 sum of the X509 certificate that will be accepted even if the "
"certificate is not trusted by a certificate authority."
msgstr ""
"Suma SHA256 Iks509 uverenja koja će biti prihvaćena čak i ako uverenju ne "
"veruje ni izdavač uverenja."

#: properties/nm-fortisslvpn-dialog.ui:142
#, fuzzy
msgid "_One time password"
msgstr "Koristi _jednokratnu lozinku"

#: properties/nm-fortisslvpn-dialog.ui:185
#, fuzzy
msgid "Advanced Properties"
msgstr "Napredne opcije za SSLVPN"

#: properties/nm-fortisslvpn-dialog.ui:193
msgid "_Cancel"
msgstr ""

#: properties/nm-fortisslvpn-dialog.ui:201
msgid "_Apply"
msgstr ""

#: properties/nm-fortisslvpn-dialog.ui:253
msgid "Show password"
msgstr "Prikaži lozinku"

#: properties/nm-fortisslvpn-dialog.ui:269
msgid "Password passed to SSLVPN when prompted for it."
msgstr "Lozinka koja se prosleđuje SSLVPN-u kada je zatraži."

#: properties/nm-fortisslvpn-dialog.ui:282
msgid ""
"Set the name used for authenticating the local system to the peer to "
"<name>.\n"
"config: user <name>"
msgstr ""
"Podesite naziv korišćen za prijavljivanje lokalnog sistema parnjaku na "
"<name>.\n"
"podešavanje: korisnik <name>"

#: properties/nm-fortisslvpn-dialog.ui:295
#, fuzzy
msgid "_Password"
msgstr "Lozinka"

#: properties/nm-fortisslvpn-dialog.ui:309
#, fuzzy
msgid "User _name"
msgstr "Korisničko ime"

#: properties/nm-fortisslvpn-dialog.ui:323
msgid "_Gateway"
msgstr "_Mrežni prolaz"

#: properties/nm-fortisslvpn-dialog.ui:369
msgid "General"
msgstr "Opšte"

#: properties/nm-fortisslvpn-dialog.ui:413
msgid "A_dvanced…"
msgstr "_Napredno…"

#: properties/nm-fortisslvpn-dialog.ui:449
msgid "Default"
msgstr "Osnovno"

#, fuzzy
#~ msgid "User _Key"
#~ msgstr "Korisnički ključ"

#, fuzzy
#~ msgid "_User Certificate"
#~ msgstr "Korisničko uverenje"

#, fuzzy
#~ msgid "_CA Certificate"
#~ msgstr "Uverenje izdavača uverenja"

#~ msgid ""
#~ "Ask for an one-time password (OTP) for two factor authentication (2FA)."
#~ msgstr ""
#~ "Traži jednokratnu lozinku (OTP) za potvrdu identiteta dva činilaca (2FA)."

#~ msgid "Could not find secrets (connection invalid, no vpn setting)."
#~ msgstr ""
#~ "Ne mogu da pronađem tajne (veza je neispravna, nema vpn podešavanja)."

#~ msgid "Invalid VPN username."
#~ msgstr "Neispravno VPN korisničko ime."
